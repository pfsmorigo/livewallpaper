DIR := "$$HOME/$(shell pwd | cut -d/ -f4-)"
TMP_FILE := "/tmp/current_cron"

image:
	DISPLAY=:0 phantomjs tradingview_usd.js

apply: get_last_image
	DISPLAY=:0 feh --bg-scale $(LAST_IMAGE)

show: get_last_image
	eog $(LAST_IMAGE)

stop:
	@crontab -l | grep -v tradingview_phantomjs > $(TMP_FILE) || true
	@crontab $(TMP_FILE)
	crontab -l

start: stop clean image apply
	# Run each 30min but only in commecial hours in weekdays
	@echo '*/30 8-18 * * 1-5 make -C $(DIR) clean image apply' >> $(TMP_FILE)
	@crontab $(TMP_FILE)
	crontab -l

get_last_image:
	$(eval LAST_IMAGE=$(shell find -name  '*.png' | sort | tail -n1))

clean:
	$(RM) tradingview*.png
