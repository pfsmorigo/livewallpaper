#!/usr/bin/env phantomjs

var fs = require('fs');
var page = require('webpage').create();
page.viewportSize = { width: 1920, height: 1080 };

page.open('file://' + fs.workingDirectory + '/tradingview_usd.html',
	function(status) {
	console.log("Status: " + status);

	if (status === "success") {
		window.setTimeout(function () {
			var date = new Date();
			var time = date.getTime();
			console.log('Status ' + status);
			page.render('tradingview_usd_' + time + '.png');
			phantom.exit();
		}, 5000);
	}
});
